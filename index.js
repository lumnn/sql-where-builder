const sqlComparators = {
  'eq': '=',
  'neq': '!=',
  'gt': '>',
  'gte': '>=',
  'lt': '<',
  'lte': '<=',
  'like': 'LIKE',
  'nlike': 'NOT LIKE'
}

function sqlWhereBuilder(query, options) {
  options = options || {}
  var defaultAlias = options.alias || ''
  var conjunction = options.conjunction || 'AND'
  var statement = []
  var parameters = []

  try {
    var keys = Object.keys(query)
  } catch (e) {
    throw new TypeError(`Expected object, got ${typeof query}`)
  }

  for (let key of keys) {
    if (key === '$or') {
      let orPart = sqlWhereBuilder(query[key], { ...options, conjunction: 'OR' })
      statement.push('(' + orPart.statement + ')')
      parameters.push(...orPart.parameters)

      continue
    }

    let comparasion = query[key]

    let isArray = Array.isArray(comparasion)
    if (isArray || typeof comparasion !== 'object') {
      comparasion = {
        type: isArray ? 'in' : 'eq',
        value: comparasion
      }
    }

    let alias = ''
    if (defaultAlias && key.indexOf('.') === -1) {
      alias = '`' + defaultAlias + '`.'
    }

    let sqlKey = `${alias}\`${key}\``

    if (sqlComparators[comparasion.type]) {
      let comparator = sqlComparators[comparasion.type]
      statement.push(`${sqlKey} ${comparator} ?`)
      parameters.push(comparasion.value)

      continue
    }

    if (comparasion.type === 'in' || comparasion.type === 'nin') {
      let comparator = comparasion.type === 'in' ? 'IN' : 'NOT IN'
      statement.push(`${sqlKey} ${comparator} (${comparasion.value.map(() => '?').join(',')})`)
      parameters.push(...comparasion.value)

      continue
    }

    if ('between' === comparasion.type && comparasion.from && comparasion.to) {
      statement.push(`${sqlKey} BETWEEN ? AND ?`)
      parameters.push(comparasion.from, comparasion.to)

      continue
    }

    if (comparasion.type === 'null' || comparasion.type === 'notnull') {
      statement.push(`${sqlKey} IS ` + (comparasion.type === 'null' ? 'NULL' : 'NOT NULL'))

      continue
    }

    throw new Error(`Unknown comparasion type ${comparasion.type}`)
  }

  statement = statement.join(` ${conjunction} `)

  return { statement, parameters }
}

module.exports = sqlWhereBuilder
