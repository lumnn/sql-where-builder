# SQL Where Builder
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](#)

> A library to build SQL WHERE statements from object.

## Install

```sh
npm install sql-where-builder
```

## Usage

```js
const sqlWhereBuilder = require('sql-where-builder')

sqlWhereBuilder({ test: 1, name: 'package' })
// {
//   statement: '`test` = ? AND `name` = ?',
//   parameters: [1, 'package']
// }
```

## Run tests

```sh
npm run test
```

## Examples

### Basic Example

```js
const sqlWhereBuilder = require('sql-where-builder')

var where = sqlWhereBuilder({ test: 1, name: 'package' })
// {
//   statement: '`test` = ? AND `name` = ?',
//   parameters: [1, 'package']
// }
```

### Or Queries

```js
sqlWhereBuilder({ $or: { status: 'completed', dispatched: true }})
// {
//   statement: '`status` = ? OR `dispatched` = ?',
//   parameters: ['completed', true]
// }
```

### Other comparasions

This requires object with type key as a value.

```js
sqlWhereBuilder({
    status: {
        type: 'neq',
        value: 'completed'
    }
})
// {
//   statement: '`status` != ?',
//   parameters: ['completed']
// }
```

#### Available types

| type | result (needed variables) | Notes |
|---|---|---|
| `eq` | = `value` | _Used by default if object value is string not object_ |
| `neq` | != `value` |  |
| `gt` | > `value` |  |
| `gte` | >= `value` |  |
| `lt` | < `value` |  |
| `lte` | <= `value` |  |
| `in` | IN(`value`) | _`value` must be array. Used by default if value is array not object_ |
| `nin` | NOT IN(`value`) | _`value` must be array_ |
| `between` | BEWTEEN `from` AND `to` |  |
| `like` | LIKE `value` |  |
| `nlike` | NOT LIKE `value` |  |
| `null` | IS NULL |  |
| `notnull` | IS NOT NULL |  |

```js
sqlWhereBuilder({
    price: {
        type: 'between',
        from: 10,
        to: 100
    }
})
// {
//   statement: '`price` BETWEEN ? AND ?',
//   parameters: [10, 100]
// }
```

## Options

### Alias

There is an option to prepend all object keys with defined alias. Keys that
have a dot already in the name won't get alias

```js
sqlWhereBuilder({
  status: 1,
  'otherAlias.test': 'other'
}, {
  alias: 'alias',
})
// {
//   statement: '`alias`.`price` = ? AND `otherAlias`.`test` = ?',
//   parameters: [1, 'other']
// }
```
