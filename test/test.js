const assert = require('assert')
const sqlWhereBuilder = require('../index.js')

describe("SQL Where Builder", function () {
  it('should create simple statement with no alias', function () {
    var where = sqlWhereBuilder({ test: 1 })

    assert.equal(where.statement, '`test` = ?')
    assert.equal(where.parameters.length, 1)
    assert.equal(where.parameters[0], 1)
  })

  it('should create AND statements when more keys provides', function () {
    var where = sqlWhereBuilder({ foo: 'one', bar: 'two' })

    assert.equal(where.statement, '`foo` = ? AND `bar` = ?')
    assert.equal(where.parameters.length, 2)
    assert.equal(where.parameters[0], 'one')
    assert.equal(where.parameters[1], 'two')
  })

  it('should handle object as value', function () {
    var where = sqlWhereBuilder({ name: { type: 'neq', value: 'John' }})

    assert.equal(where.statement, '`name` != ?')
    assert.equal(where.parameters.length, 1)
    assert.equal(where.parameters[0], "John")
  })

  it('should handle array as a value, and buid IN() query', function () {
    var where = sqlWhereBuilder({ status: ['open', 'ready'] })

    assert.equal(where.statement, '`status` IN (?,?)')
    assert.equal(where.parameters.length, 2)
    assert.equal(where.parameters[0], 'open')
    assert.equal(where.parameters[1], 'ready')
  })

  it('should handle in an nin types', function () {
    var where = sqlWhereBuilder({
      status: {
        type: 'in',
        value: ['open', 'ready']
      },
      scan: {
        type: 'nin',
        value: ['failed', 'pending']
      }
    })

    assert.equal(where.statement, '`status` IN (?,?) AND `scan` NOT IN (?,?)')
    assert.equal(where.parameters.length, 4)
    assert.equal(where.parameters[0], 'open')
    assert.equal(where.parameters[1], 'ready')
    assert.equal(where.parameters[2], 'failed')
    assert.equal(where.parameters[3], 'pending')
  })

  it('should build or statements for $or keys', function () {
    var where = sqlWhereBuilder({
      status: true,
      $or: {
        name: 'Test',
        description: 'Test'
      }
    })

    assert.equal(where.statement, '`status` = ? AND (`name` = ? OR `description` = ?)')
    assert.equal(where.parameters.length, 3)
    assert.equal(where.parameters[0], true)
    assert.equal(where.parameters[1], 'Test')
    assert.equal(where.parameters[2], 'Test')
  })

  it('builds query with alias if provided', function () {
    var where = sqlWhereBuilder({
      status: 1,
      'otherAlias.test': 'other'
    }, {
      alias: 'alias',
    })

    assert.equal(
      where.statement,
      '`alias`.`status` = ? AND `otherAlias.test` = ?'
    )
  })

  it('can create BETWEEN() query', function () {
    var where = sqlWhereBuilder({
      value: {
        type: 'between',
        from: 10,
        to: 20,
      }
    })

    assert.equal(
      where.statement,
      '`value` BETWEEN ? AND ?'
    )
    assert.equal(where.parameters.length, 2)
  })

  it('can create IS NULL and IS NOT NULL query', function () {
    var where = sqlWhereBuilder({
      isNull: {
        type: 'null',
      },
      isNotNull: {
        type: 'notnull',
      }
    })

    assert.equal(
      where.statement,
      '`isNull` IS NULL AND `isNotNull` IS NOT NULL'
    )
    assert.equal(where.parameters.length, 0)
  })

  it('throws error when unknown comparasion type', function () {
    assert.throws(
      () => sqlWhereBuilder({
        value: {
          type: 'similar',
          value: 'Jhon'
        }
      })
    )
  })

  it('throws error when non object argument is passed', function () {
    assert.throws(() => sqlWhereBuilder(undefined))
  })
})
